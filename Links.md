# Scheduler Algorithmen

## Allgemein

* https://man7.org/linux/man-pages/man7/sched.7.html

## DEADLINE

* https://www.kernel.org/doc/html/latest/scheduler/sched-deadline.html
* https://lwn.net/Articles/793392/

## EEVDF

* https://lwn.net/Articles/925371/
* https://www.phoronix.com/news/Linux-6.6-EEVDF-Merged

# PREEMPT_RT

* https://wiki.linuxfoundation.org/realtime/start
* https://lwn.net/Articles/146861/
* https://wiki.linuxfoundation.org/realtime/documentation/known_limitations

# Hardwareanforderungen

* https://www.kernel.org/doc/html/latest/admin-guide/mm/nommu-mmap.html
* https://openwrt.org/toh/start
* https://openwrt.org/supported_devices/432_warning